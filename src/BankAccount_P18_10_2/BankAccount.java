package BankAccount_P18_10_2;

public class BankAccount {
	 private double balance;

	 public BankAccount(){   
	    balance = 0;
	 }

	 public void deposit(double amount){  
	    double newBalance = balance + amount;
	    balance = newBalance;
	 }

	 public void withdraw(double amount){   
	    double newBalance = balance - amount;
	    balance = newBalance;
	 }

	 public void setBalance(double balance) {
		 this.balance = balance;
	 }

	 public double getBalance(){   
		 return balance;
	 }
}
