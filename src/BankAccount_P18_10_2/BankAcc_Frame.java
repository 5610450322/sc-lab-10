package BankAccount_P18_10_2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class BankAcc_Frame {
	BankAccount bank = new BankAccount();
	private JPanel panel1;
	private JPanel panel2;
	private JTextField text1;
	private JTextField text2;
	private JTextArea text3;
	private JButton button1;
	private JButton button2;
	private JButton button3;
	private JLabel label1;
	private JLabel label2;
	private JLabel label3;
	private JLabel label4;
	private JLabel label5;
	private String str = "";
	
	public void creatFrame(){
		JFrame j = new JFrame("BankAccount");
		j.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		j.setSize(500, 270);
		j.setLayout(null);
			
		label1 = new JLabel("Enter amount for deposit."); 
		label1.setBounds(13, 15, 200, 30);
			
		label2 = new JLabel("Enter amount for withdraw."); 
		label2.setBounds(13, 95, 200, 20);
			
		text1 = new JTextField();
		text1.setBounds(13, 126, 150, 23);
		
		text2 = new JTextField();
		text2.setBounds(13, 50, 150, 23);
			
		button1 = new JButton("Enter"); 
		button1.setBounds(180, 126, 90, 23);
		button1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String amount = text1.getText();
				bank.withdraw(Integer.parseInt(amount));
				label3.setText("Your balance  :   " + bank.getBalance() + "   Baht");
				str = str + "You withdraw  :   " + bank.getBalance() + "   Baht" + "\n";
				text3.setText(str);
			}
		});
			
		button2 = new JButton("Enter"); 
		button2.setBounds(180, 50, 90, 23);
		button2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String amount = text2.getText();
				bank.deposit(Integer.parseInt(amount));
				label3.setText("Your balance  :   " + bank.getBalance() + "   Baht");
				str = str + "You deposit  :   " + bank.getBalance() + "   Baht" + "\n";
				text3.setText(str);
		    }
		});
		
		button3 = new JButton("Clear"); 
		button3.setBounds(180, 180, 90, 23);
		button3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				text2.setText("");
				text1.setText("");
				label3.setText("Your balance  :   " + 0 + "   Baht");
				text3.setText("");
				str = "";
				bank.setBalance(0);
		    }
		});
		
		label3 = new JLabel("Your balance  :   " + bank.getBalance() + "   Baht"); 
		label3.setBounds(13, 175, 200, 30);
		
		label4 = new JLabel("Account"); 
		label4.setBounds(5, 3, 200, 20);
		
		label5 = new JLabel("-- -- -- -- -- -- -- -- -- -- -- --"); 
		label5.setBounds(5, 20, 200, 20);
		
		text3 = new JTextArea();
		text3.setBounds(5, 40, 183, 188);
		
		panel1 = new JPanel();
		panel1.setBounds(5, 6, 285, 233);
		panel1.setLayout(null);
		panel1.setBorder(BorderFactory.createEtchedBorder());
		panel1.add(label1);
		panel1.add(label2);
		panel1.add(label3);
		panel1.add(text1);
		panel1.add(text2);
		panel1.add(button1);
		panel1.add(button2);
		panel1.add(button3);
		
		panel2 = new JPanel();
		panel2.setBounds(295, 6, 193, 233);
		panel2.setLayout(null);
		panel2.setBorder(BorderFactory.createEtchedBorder());
		panel2.add(label4);
		panel2.add(label5);
		panel2.add(text3);
		
		j.add(panel1);
		j.add(panel2);
		j.setVisible(true);
		j.setResizable(false);
	}
}
