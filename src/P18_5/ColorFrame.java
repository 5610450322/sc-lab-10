package P18_5;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class ColorFrame {
	private JPanel panel1;
	private JButton button1;
	private JComboBox combobox;
	
	public void creatFrame(){
		JFrame j = new JFrame("Color Frame");
		j.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		j.setSize(400, 270);
		j.setLayout(new BorderLayout());
	
		button1 = new JButton("OK");
		button1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (combobox.getSelectedItem() == "Red"){
					panel1.setBackground(Color.RED);
				}
				else if (combobox.getSelectedItem() == "Green"){
					panel1.setBackground(Color.GREEN);
				}
				else if (combobox.getSelectedItem() == "Blue"){
					panel1.setBackground(Color.BLUE);
				}
			}
		});
		
		combobox = new JComboBox();
		combobox.addItem("Red");
		combobox.addItem("Green");
		combobox.addItem("Blue");
		
		FlowLayout flowlyt = new FlowLayout();
		panel1 = new JPanel();
		panel1.setLayout(flowlyt);
		panel1.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
		flowlyt.setVgap(200);
		panel1.add(combobox);
		panel1.add(button1);
		
		j.add(panel1, BorderLayout.CENTER);
		j.setVisible(true);
		j.setResizable(false);
	}
}
