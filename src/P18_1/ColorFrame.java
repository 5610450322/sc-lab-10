package P18_1;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class ColorFrame extends JFrame{
	private JPanel panel1;
	private JButton button1;
	private JButton button2;
	private JButton button3;
	
	public void creatFrame(){
		JFrame j = new JFrame("Color Frame");
		j.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		j.setSize(400, 270);
		j.setLayout(new BorderLayout());
		
		button1 = new JButton("Red");
		button1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel1.setBackground(Color.RED);
			}
		});
		
		button2 = new JButton("Green");
		button2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel1.setBackground(Color.GREEN);
			}
		});
		
		button3 = new JButton("Blue");
		button3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel1.setBackground(Color.BLUE);
			}
		});
		
		FlowLayout flowlyt = new FlowLayout();
		panel1 = new JPanel();
		panel1.setLayout(flowlyt);
		panel1.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
		flowlyt.setVgap(200);
		panel1.add(button1);
		panel1.add(button2);
		panel1.add(button3);
		
		j.add(panel1, BorderLayout.CENTER);
		j.setVisible(true);
		j.setResizable(false);
	}
	
	
}
