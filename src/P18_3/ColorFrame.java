package P18_3;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class ColorFrame extends JFrame{
	private JPanel panel1;
	private JButton button1;
	private JRadioButton radio1;
	private JRadioButton radio2;
	private JRadioButton radio3;
	
	public void creatFrame(){
		JFrame j = new JFrame("Color Frame");
		j.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		j.setSize(400, 270);
		j.setLayout(new BorderLayout());
		
		button1 = new JButton("OK");
		button1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (radio1.isSelected()){
					panel1.setBackground(Color.RED);
				}
				else if (radio2.isSelected()){
					panel1.setBackground(Color.GREEN);
				}
				else {
					panel1.setBackground(Color.BLUE);
				}
			}
		});
		
		radio1 = new JRadioButton("Red");
		radio2 = new JRadioButton("Green");
		radio3 = new JRadioButton("Blue");
		
		ButtonGroup group = new ButtonGroup();
		group.add(radio1);
		group.add(radio2);
		group.add(radio3);
		
		FlowLayout flowlyt = new FlowLayout();
		panel1 = new JPanel();
		panel1.setLayout(flowlyt);
		panel1.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
		flowlyt.setVgap(200);
		panel1.add(radio1);
		panel1.add(radio2);
		panel1.add(radio3);
		panel1.add(button1);
		
		j.add(panel1, BorderLayout.CENTER);
		j.setVisible(true);
		j.setResizable(false);
	}
}
