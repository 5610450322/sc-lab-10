package P18_6;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

public class ColorFrame extends JFrame{
	private JPanel panel1;
	private JMenu menu1;
	private JMenu menu2;
	private JMenuBar menuBar;
	private JMenuItem menuItem;
	
	public void creatFrame(){
		JFrame j = new JFrame("Color Frame");
		j.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		j.setSize(400, 270);
		j.setLayout(new BorderLayout());
	
		menuBar = new JMenuBar();
		menu1 = new JMenu("Edit");
		menu2 = new JMenu("Color");
		menu2.add(createColorBackgorund("Red"));
		menu2.add(createColorBackgorund("Green"));
		menu2.add(createColorBackgorund("Blue"));
		
		menu1.add(menu2);
		menuBar.add(menu1);
		
		FlowLayout flowlyt = new FlowLayout();
		panel1 = new JPanel();
		panel1.setLayout(flowlyt);
		panel1.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
		
		j.add(panel1);
		j.setJMenuBar(menuBar);
		j.setVisible(true);
		j.setResizable(false);
	}
	
	public JMenuItem createColorBackgorund(String color){
		menuItem = new JMenuItem(color);
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (color == "Red"){
					panel1.setBackground(Color.RED);
				}
				else if (color == "Green"){
					panel1.setBackground(Color.GREEN);
				}
				else if (color == "Blue"){
					panel1.setBackground(Color.BLUE);
				}
			}
		});
		return menuItem;
	}
}
