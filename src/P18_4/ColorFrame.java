package P18_4;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class ColorFrame extends JFrame{
	private JPanel panel1;
	private JButton button1;
	private JCheckBox  check1;
	private JCheckBox  check2;
	private JCheckBox  check3;
	
	public void creatFrame(){
		JFrame j = new JFrame("Color Frame");
		j.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		j.setSize(400, 270);
		j.setLayout(new BorderLayout());
		
		button1 = new JButton("OK");
		button1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (check1.isSelected() && check2.isSelected() && check3.isSelected()){
					panel1.setBackground(Color.DARK_GRAY);
				}
				else if (check1.isSelected() && check2.isSelected()){
					panel1.setBackground(Color.YELLOW);
				}
				else if (check1.isSelected() && check3.isSelected()){
					panel1.setBackground(Color.ORANGE);
				}
				else if (check2.isSelected() && check3.isSelected()){
					panel1.setBackground(Color.PINK);
				}
				else if (check1.isSelected()){
					panel1.setBackground(Color.RED);
				}
				else if (check2.isSelected()){
					panel1.setBackground(Color.GREEN);
				}
				else if (check3.isSelected()){
					panel1.setBackground(Color.BLUE);
				}
				
			}
		});
		
		check1 = new JCheckBox("Red");
		check2 = new JCheckBox("Green");
		check3 = new JCheckBox("Blue");
		
		FlowLayout flowlyt = new FlowLayout();
		panel1 = new JPanel();
		panel1.setLayout(flowlyt);
		panel1.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
		flowlyt.setVgap(200);
		panel1.add(check1);
		panel1.add(check2);
		panel1.add(check3);
		panel1.add(button1);
		
		j.add(panel1, BorderLayout.CENTER);
		j.setVisible(true);
		j.setResizable(false);
	}
}
